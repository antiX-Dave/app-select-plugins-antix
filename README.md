# app-select-plugins-antix

Repository / package for various app-select plugins

- app-select.conf should be placed in the patch directory as app-select.conf.mod. 
This will be used in the post/pre install script to patch the plugins into the original app-select.conf from the app-select package.

- Plugins should be placed in the mirrored directory for where it should be placed on install. (/usr/lib/app-select/plugins)
These should be read / write for root and exec for regular users
These files can be symbolic links to other commands in the filesystem.

- app-select uses a second gettext domain for translations.
The name of the gettext domain is app-select-plugins
These should be placed in the ./usr/share/locale/ directory.
