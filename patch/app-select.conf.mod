#Note: For some security on exec commands, commands will only be accepted if in /usr/lib/app-select/plugins/
#These should be of a similar permisson type 711 to avoid user modification and injection of commands under a regular user
#Exec files can be linked in this directory. EX: leafpad (ln -s /usr/bin/leafpad /usr/lib/plugins/app-select/leafpad)

# NOTE: AFTER ANY CHANGES, COPY THIS FILE TO ~/.config/app-select.conf

#Fill in / request values
# %n = name of item
# %e = executable
# %c = categories
# %f = filename
# %p = filepath (name included)
# %t = terminal (true/false)
# %i = icon

Open in leafpad|leafpad "%p"
View icon|mirage "%i"
Copy to Desktop|copy-to-desktop "%p" "%n"
Add to IceWM Toolbar|toolbar-manager "%n" "%e" "%i" "%t"
Add to Personal menu|add-to-personal-menu "%p" "%n" "%e" "%i" "%t"
#Remove from toolbar|toolbar-manager "%n" "%e"
